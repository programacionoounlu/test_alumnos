package TP1;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;



@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TP1Tests {
	TP1 tp;
	
	@Before
	public void setUp() {
		tp = new TP1();
	}
	
	// Ejercicio 01
	@Test
	public void testEj01CalcularAreaRectanguloParametrosCorrectos() {
		assertEquals(8, tp.calcularAreaRectangulo(2, 4), 0.001);
	}
	
	// Ejercicio 02
	@Test
	public void testEj02CalcularPerimetroRectanguloParametrosCorrectos() {
		assertEquals(12, tp.calcularPerimetroRectangulo(2, 4), 0.001);
	}
	
	// Ejercicio 03
	@Test
	public void testEj03ConvertirMetroAPulgadaParametrosCorrectos() {
		assertEquals(39.370, tp.convertirMetroAPulgada(1), 0.001);
	}
	
	// Ejercicio 04
	public void testEj04CalcularAreaRomboParametrosCorrectos() {
		assertEquals(3.071, tp.calcularAreaRombo(1.345, 4.567), 0.001);
	}
	
	// Ejercicio 05
	@Test
	public void testEj05VelocidadAutomovilParametrosCorrectos() {
		assertEquals(0.7, tp.velocidadAutomovil(30, 30, 56, 356.123), 0.001);
	}
	
	// Ejercicio 06
	@Test
	public void testEj06SumarCifrasParametrosCorrectos() {
		assertEquals(24, tp.sumarCifras(3975));
	}
	
	
	//Ejercicio 07
	@Test
	public void testEj07DeterminarAprobadosDesaprobadosAusentesParametrosCorrectos() {
		tp.establecerResultados(50, 34, 1);
		assertEquals(68.0, tp.determinarAprobados(), 0.001);
	}

	// Ejercicio 08
	@Test
	public void testEj08HipotenusaParametrosCorrectos() {
		assertEquals(9.875, tp.hipotenusa(2.451, 9.567), 0.001);
	}
	
	// Ejercicio 09
	@Test
	public void testEj09PerimetroTEquilateroParametrosCorrectos() {
		assertEquals(17.85, tp.perimetroTEquilatero(5.95), 0.001);
	}
}

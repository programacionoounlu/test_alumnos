package TP2;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TP2Test {

	// Instancia para testeo
	private TP2 tp2;

	// Reinstancio antes de cada m�todo de test
	@BeforeEach
	void setUp() {
		tp2 = new TP2();
	}

	
	@Test
	void testEj1CalcularPrimosParametrosCorrectos() {

		// Assert con el ejemplo del enunciado (primos hasta el 7)
		Integer[] resultadoEsperado = { 5, 3, 2 };
		Integer[] resultadoReal = tp2.calcularPrimos(7);
		Arrays.sort(resultadoReal);
		Arrays.sort(resultadoEsperado);
		Assert.assertArrayEquals("Fall� la operaci�n calcularPrimos(7)", resultadoEsperado, resultadoReal);
	}
	
	@Test
	void testEj2CalcularPerfectosParametrosCorrectos() {

		// Assert con el ejemplo del enunciado (perfectos hasta el 30)
		Integer[] resultadoEsperado = { 6, 28 };
		Integer[] resultadoReal = this.tp2.calcularPerfectos(30);
		Arrays.sort(resultadoReal);
		Arrays.sort(resultadoEsperado);
		
		Assert.assertArrayEquals("Fall� la operaci�n calcularPerfectos(30)", resultadoEsperado, resultadoReal);
		
		
		
	}
	
	@Test
	void testEj3CalcularFactorialParametrosCorrectos() {	
		Assert.assertEquals("Fall� la operaci�n calcularFactorial(5)", 120, this.tp2.calcularFactorial(5));		
	}
	
	
	@Test
	void testEj4CalcularFibonacciParametrosCorrectos() {
		// Assert con el ejemplo del enunciado (fibonacci hasta 8)
		int[] resultadoEsperado = { 0, 1, 1, 2, 3, 5, 8, 13 };
		int[] resultadoReal = this.tp2.calcularFibonacci(8);
		Arrays.sort(resultadoReal);
		Arrays.sort(resultadoEsperado);
		Assert.assertArrayEquals("Fall� la operaci�n calcularFibonacci(8)", resultadoEsperado, resultadoReal);
	}
	
	
	}

	@Test
	void testEj52CalcularPositivos(){
		// Assert para calcularPositivos() del ejemplo del enunciado
		int[] ejemplo = { 1, 5, -1, 2, 1, -4 };
		tp2.setVector(ejemplo);
		int[] arregloPrueba = { 1, 5, 2, 1 };
		Assert.assertArrayEquals("Fall� el calcularPositivos() de {1, 5, -1, 2, 1, -4}.", arregloPrueba,
				tp2.calcularPositivos());
	}

	@Test
	void testEj53ContarMayoresQue() {
		// Assert para contarMayoresQue() del ejemplo del enunciado
		int[] ejemplo = { 1, 5, -1, 2, 1, -4 };
		tp2.setVector(ejemplo);
		Assert.assertEquals("Fall� el contarMayoresQue(2) de {1, 5, -1, 2, 1, -4}.", 1, tp2.contarMayoresQue(2));
		
		Assert.assertEquals("Fall� el contarMayoresQue(2) de {1, 5, -1, 2, 1, -4}.", 4, tp2.contarMayoresQue(-1));

	}

	@Test
	void testEj54CalcularMayor() {
		int[] ejemplo = { 1, 5, -1, 2, 1, -4 };
		tp2.setVector(ejemplo);
		Assert.assertEquals("Fall� el calcularMayor() de {1, 5, -1, 2, 1, -4}.", 5, tp2.calcularMayor());

	
	}

	
	void testEj55MayorFrecuencia() {

		int[] ejemplo = { 1, 5, -1, 2, 1, -4 };
		Integer[] esperado = { 1 };
		tp2.setVector(ejemplo);
		
		Assert.assertArrayEquals("Fall� el mayorFrecuencia() de {1, 5, -1, 2, 1, -4}.", esperado,
				tp2.mayorFrecuencia());


	
	}

	@Test
	void testEj56Media() {
		int[] ejemplo = { 1, 5, -1, 2, 1, -4 };
		tp2.setVector(ejemplo);
		Assert.assertEquals("Fall� el media() de { 1, 5, -1, 2, 1, -4 }.", 0.6666, tp2.media(), 0.001);
	}

	@Test
	void testEj57Desvio() {
		// Assert para desvio() del ejemplo del enunciado
		int[] ejemplo = { 1, 5, -1, 2, 1, -4 };
		double[] esperado = {0.3334,4.3334, 1.6666, 1.3334, 0.3334, 4.6666};
		tp2.setVector(ejemplo);
		Assert.assertArrayEquals("Fall� el desvio() de { 1, 5, -1, 2, 1, -4 }.", esperado, tp2.desvio(), 0.001);


	}

	@Test
	void testEj58BuscarEnInt() {
		// Assert para buscarEn() del ejemplo del enunciado
		int[] ejemplo = { 1, 5, -1, 2, 1, -4 };
		tp2.setVector(ejemplo);
		Assert.assertEquals("Fall� el buscarEn() de -4 en { 1, 5, -1, 2, 1, -4 }.", -4, tp2.buscarEn(-4));

	}

	

	// Ejercicio 5.10: buscarEn()
	@Test
	void testEj510BuscarEnString() {
		// Assert para buscarEn() del ejemplo del enunciado
		String[] ejemplo = { "Texto 1", "Otro texto", "Hola", "mundo" };
		tp2.setVector(ejemplo);
		System.out.println(tp2.buscarEn("Texto 1"));
		Assert.assertEquals("Fall� el buscarEn() de 'Texto 1' en { \"Texto 1\", \"Otro texto\", \"Hola\", \"mundo\" }.",
				"Texto 1", tp2.buscarEn("Texto 1"));

	}

}
